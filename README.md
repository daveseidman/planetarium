# Planetarium

Oculus Relaxation App intended to be used while lying down. Use passthrough cameras to reveal the current night sky on user's bedroom ceiling. Users can stargaze / count sheep / and do other relaxing activities

## Notes

- Domain acquired: arium.site, set up subdomian and host at: 
http://planet.arium.site

## R&D TODO

- Will tracking work while lying down?
- Can we access user's ceiling rough geometry?
- Do passthrough view work in webxr? 

